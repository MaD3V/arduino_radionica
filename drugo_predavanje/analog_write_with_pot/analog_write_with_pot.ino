const int pot = A0;	// Potenciometar je povezan na analogni pin A0
const int led_pin = 3;	// Dioda je povezana na pin 3
int value;	// Prati očitani napon

void setup()
{
        pinMode(led_pin, OUTPUT);	// Inicijalizuje pin 3 kao izlaz
}

void loop()
{
	value = analogRead(pot);	// Čita vrednost sa potenciometra
	value = map(value, 0, 1024, 0, 255);	// Prebacuje vrednost u rang 0-255
	analogWrite(led_pin, value);	// Izbacuje očitanu vrednost PWM-a na diodu
	delay(1);	// Pauza pre sledećeg čitanja
}
