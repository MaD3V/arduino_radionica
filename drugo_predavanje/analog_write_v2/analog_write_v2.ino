const int led_pin = 3;	// Dioda je povezana na pin 3
int i = 0;	// Brojač za PWM
int smer = 1;	// Prati da li PWM raste ili opada

void setup()
{
        pinMode(led_pin, OUTPUT);	// Inicijalizuje pin 3 kao izlaz
}

void loop()
{
        analogWrite(led_pin, i);	// Izbacuje PWM signal na pinu

	switch (i) {
	case 0:		// Ako je stiglo do nule, počinje da uvećava
		smer = 1;
		break;
	case 255:	// Ako je stiglo do maksimuma počinje da smanjuje
		smer = -1;
		break;
	}

	i = i + smer;

        delay(5);	// Pauza (koliko dugo svetli na jednoj vrednosti PWM)
}
