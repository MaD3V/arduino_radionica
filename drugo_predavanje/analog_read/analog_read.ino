const int pot = A0;	// Klizač potenciometra povezan na pin A0
int value;

void setup()
{
	Serial.begin(9600);	// Inicijalizuje serijsku konekciju na brzini od 9600 bit/s
}

void loop()
{
	value = analogRead(pot);	// Očitava vrednost u promenljivu
	//float new_value = (value * 5.0) / 1024.0;	// Mapira vrednost na 0-5 V
	Serial.println(value);	// Štampa vrednost
	delay(1);	// Čeka jednu milisekundu (radi stabilnosti)
}
