const int led_pin = 3;	// Dioda je povezana na pin 3
int i = 0;	// Brojač za PWM

void setup()
{
	pinMode(led_pin, OUTPUT);	// Inicijalizuje pin 3 kao izlaz
}

void loop()
{
	analogWrite(led_pin, i);	// Izbacuje PWM signal na pinu
	i++;
	if (i > 255)	// Kada stigne do maksimuma, vraća brojač na početak
		i = 0;

	delay(10);	// Pauza (koliko dugo svetli na jednoj vrednosti PWM)
}
