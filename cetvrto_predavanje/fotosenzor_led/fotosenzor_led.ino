const int fotootpornik = A0;	// Fotootpornik je povezan na analogni pin A0
const int led = 13;	// Dioda povezana na pin 13
int value;	// Prati napon na fotootporniku

void setup()
{
	pinMode(led, OUTPUT);	// Inicijalizuje pin 13 kao izlaz
	digitalWrite(led, LOW);	// Postavlja diodu kao isključenu (za svaki slučaj)
        Serial.begin(9600);	// Inicijalizuje komunikaciju sa računarom pri brzini od 9600 bit/s
}

void loop()
{
        value = analogRead(fotootpornik);	// Čita napon na otporniku
        Serial.println(value);	// Štampa vrednost na serijski monitor
	if (value < 200)	// Ako je vrednost manja od 200 uključi led
		digitalWrite(led, HIGH);
	else			// Ako nije, isključi led
		digitalWrite(led, LOW);
        delay(50);		// Pauza između očitavanja
}

