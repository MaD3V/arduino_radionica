const int fotootpornik = A0;	// Fotootpornik povezan na analogni pin A0
int value;	// Prati vrednost napona na otporniku

void setup()
{
	Serial.begin(9600);	// Inicijalizuje komunikaciju sa računarom pri brzini od 9600 bit/s
}

void loop()
{
	value = analogRead(fotootpornik);	// Čita vrednost na otporniku
	Serial.println(value);	// Štampa tu vrednost na serijski monitor
	delay(50);	// Pauza
}
