const int temp_sensor = A0;	// Senzor je povezan na A0

float sensor_value;
float voltage;
float temperaturaK = 0;
float temperatura = 0;

void setup()
{
	Serial.begin(9600);
}

void loop()
{
	sensor_value = analogRead(temp_sensor);	// Očitana vrednost sa ADC-a
	voltage = sensor_value * 5000 / 1024;	// Napon u milivoltima
	temperaturaK = voltage / 10;	// Temperatura u kelvinima
	temperatura = temperaturaK - 273.16;	// Temperatura u celzijusima
	Serial.println(temperatura);
	delay(100);
}
