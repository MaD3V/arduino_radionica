const int led_r = 9;
const int led_g = 10;
const int led_b = 11;
const int touch_senzor = 12;
const int taster = 13;
const int pot = A0;

float pot_value;

/*
MOJA LED JE COMMON ANODE, TAKO DA SU VREDNOSTI ZA UPIS INVERTOVANE
AKO TREBA DA SVETLI CRVENA NA TOM PINU ĆE BITI NULA, A NA OSTALA DVA
ĆE BITI 255 (5 V)
*/
void setup()
{
	pinMode(led_r, OUTPUT);
	pinMode(led_g, OUTPUT);
	pinMode(led_b, OUTPUT);
	pinMode(touch_senzor, INPUT);
	pinMode(taster, INPUT);

	analogWrite(led_r, 255);
	analogWrite(led_g, 255);
	analogWrite(led_b, 255);
}

void loop()
{
	if (digitalRead(touch_senzor) == HIGH){
		while (digitalRead(taster) != HIGH){
			// Ljubičasta
			pot_value = analogRead(pot);
			setColor(map(pot_value, 0, 1023, 255, 0), 255, map(pot_value, 0, 1023, 255, 0));
		}
	}
	if (digitalRead(taster) == HIGH){
		while (digitalRead(touch_senzor) != HIGH){
			// Žuta
			pot_value = analogRead(pot);
			setColor(255 - 255*pot_value/1023, 255 - 255*pot_value/1023, 255);
			//setColor(map(pot_value, 0, 1023, 255, 0), map(pot_value, 0, 1023, 255, 0), 255);
		}
	}
	delay(1);
}


void setColor(int red_value, int green_value, int blue_value)
{
	analogWrite(led_r, red_value);
	analogWrite(led_g, green_value);
	analogWrite(led_b, blue_value);
}
