int led_r = 9;
int led_g = 10;
int led_b = 11;

int button = 7;
int ptm = A0;

int color;
int value;

bool pressed;

void setup() {
pinMode(led_r, OUTPUT);
pinMode(led_g, OUTPUT);
pinMode(led_b, OUTPUT);

pinMode(button, INPUT);
pinMode(ptm, INPUT);

analogWrite(led_r, 0);
analogWrite(led_g, 0);
analogWrite(led_b, 0);

color = 0;
pressed = false;

Serial.begin(9600);
}

void loop() {
if (digitalRead(button) == HIGH){
if (pressed == false){
color = (color + 1) % 3;
pressed = true;
}
}
if (digitalRead(button) == LOW){
pressed = false;
}
value = analogRead(ptm);
if (value < 20) value = 0;
value = map(value, 0, 1023, 0, 255);
delay(1);

if(color == 0){
analogWrite(led_r, value);
}
if(color == 1){
analogWrite(led_g, value);
}
if(color == 2){
analogWrite(led_b, value);
}
Serial.println(value);
delay(100);
}
