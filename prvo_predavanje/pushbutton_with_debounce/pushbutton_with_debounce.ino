
int button = 2;	// Taster je povezan na pin 2

int last_button_state = LOW;	// Prethodno stanje tastera
int button_state = LOW;			// Trenutno stanje tastera

unsigned long last_button_millis = 0;
unsigned long debounce_interval = 50;	// Interval za ignorisanje dogadjaja na tasteru

void setup() {
	pinMode(button, INPUT);	// Inicijalizuje pin 2 kao ulaz

	Serial.begin(9600);		// Inicijalizuje serijsku konekciju na brzini od 9600 bit/s
}

void loop() {
	int reading = digitalRead(button);	// Gleda u kom je stanju taster

	if (reading != last_button_state){	// Ako se stanje tastera promenilo
		last_button_millis = millis();	// Pocni da meris vreme
	}
	if (millis() - last_button_millis >= debounce_interval){	// Ako je proslo vise od "debounce_interval" milisekundi
		if (reading != button_state){							// i ocitavanje je razlicito od ocekivanog stanja tastera
			button_state = reading;								// znaci da je ocitano stanje ono koje zelimo na tasteru, a ne neki sum
									// Stampa stanje tastera
		}
	}
Serial.println(button_state);
	last_button_state = reading;		// Vazno je podesiti da je "prethodno stanje" tastera za sledecu iteraciju ono sadasnje u ovoj iteraciji
	delay(1);
}
