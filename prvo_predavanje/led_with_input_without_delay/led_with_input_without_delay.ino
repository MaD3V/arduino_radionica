
int led_pin = 13;   // Dioda je povezana na pin 13
int button = 2;     // Taster je povezan na pin 2

int interval1 = 200; // Interval brzog blinkanja
int interval2 = 2000; // Interval sporog blinkanja
int interval3 = 1000;
int interval[] = {interval1, interval2, interval3};	// Niz iz kog cemo birati rezim blinkanja
int i = 0;		// Brojac kojim pratimo u kom smo rezimu blinkanja

int led_state = LOW;	// Prati stanje diode
int last_button_state = LOW;	// Prethodno stanje tastera
int button_state = LOW;				// Trenutno stanje tastera

unsigned long previous_millis = 0;
unsigned long last_button_millis = 0;
unsigned long debounce_inteval = 50;	// Interval za ignorisanje dogadjaja na tasteru

void setup() {
	pinMode(led_pin, OUTPUT); // Inicijalizuje pin 13 kao izlaz
	pinMode(button, INPUT);   // Inicijalizuje pin 2 kao ulaz

//	Serial.begin(9600);       // Inicijalizuje serijsku konekciju na brzini od 9600 bit/s
}

void blink_LED(int interval);

void loop() {
	int reading = digitalRead(button);	// Gleda u kom je stanju taster
	
	if (reading != last_button_state){	// Ako se stanje tastera promenilo
		last_button_millis = millis();	// Pocni da meris vreme
	}
	if (millis() - last_button_millis >= debounce_inteval){	// Ako je proslo vise od "debounce_interval" milisekundi
		if (reading != button_state){						// i ocitavanje je razlicito od ocekivanog stanja tastera
			button_state = reading;							// znaci da je ocitano stanje koje zelimo na tasteru, a ne neki sum
			if (button_state == HIGH){						// Ako je to stanje HIGH, odnosno pritisnut taster
				i++;										// Biramo sledeci recim blinkanja
				if (i > (sizeof(interval) / sizeof(int) - 1)){	// Kada dodje do zadnjeg rezima, pocne ispocetka
					i = 0;
				}
			}
		}
	}

	last_button_state = reading;							// Vazno je podesiti da je "prethodno stanje" tastera za sledecu iteraciju ono sadasnje u ovoj iteraciji
	blink_LED(interval[i]);									// Poziva funkciju blinkanja sa zeljenim intervalom
}

void blink_LED(int interval){						// Funckija zaduzena za blinkanje diode bez delay() funkcije
	if (millis() - previous_millis >= interval){
		previous_millis = millis();
		led_state = !led_state;
		/*
		if (led_state == LOW){
			led_state = HIGH;
		}
		else{
			led_state = LOW;
		}
		*/
		digitalWrite(led_pin, led_state);
	}
}
