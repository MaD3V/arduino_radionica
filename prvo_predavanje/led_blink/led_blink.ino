
int led_pin = 13;	// Pin na koji je povezana dioda

void setup() {
	pinMode(led_pin, OUTPUT);	// Postavlja pin 13 kao izlazni pin
}

void loop() {
	digitalWrite(led_pin, HIGH);	// Pise logicku jedinicu na led_pin
	delay(1000);					// Pauza u milisekundama
	digitalWrite(led_pin, LOW);		// Pise logicku nulu na led_pin
	delay(1000);					// Pauza u milisekundama
}
