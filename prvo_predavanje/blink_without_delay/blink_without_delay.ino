
int led_pin = 13;	// Pin na koji je povezana dioda
const long interval = 100;		// Interval koji ce da pauzira blinkanje diode
unsigned long previous_millis = 0; 	// Referentno vreme
int led_state = LOW;	// Stanje koje cemo pisati na izlazni pin
unsigned int current_millis;	// Trenutno vreme

void setup() {
	pinMode(led_pin, OUTPUT);	// Postavlja diodu kao izlaz
}

void loop() {
	current_millis = millis();	// Pogleda koliko je trenutno vreme (od ukljucivanja mikrokontrolera)
	if (current_millis - previous_millis >= interval){	// Ako je od "pocetnog" do sadasnjeg trenutka proslo <interval> milisekundi
		previous_millis = current_millis;				// "Pocetno" vreme postaje trenutno vreme
		led_state = !led_state;							// promeni vrednost stanja diode
		digitalWrite(led_pin, led_state);				// i "upisi" je na pin
	}
}
