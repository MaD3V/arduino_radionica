
int led_pin = 13;   // Dioda je povezana na pin 13
int button = 2;     // Taster je povezan na pin 2
int interval1 = 200; // Interval brzog blinkanja
int interval2 = 2000; // Interval sporog blinkanja

void setup() {
	pinMode(led_pin, OUTPUT); // Inicijalizuje pin 13 kao izlaz
	pinMode(button, INPUT);   // Inicijalizuje pin 2 kao ulaz

//  Serial.begin(9600);       // Inicijalizuje serijsku konekciju na brzini od 9600 bit/s
}

void loop() {
	digitalWrite(led_pin, HIGH);  // Blinkanje diode
	delay(interval1);
	digitalWrite(led_pin, LOW);
	delay(interval1);
	if (digitalRead(button) == HIGH){   // Ako je pritisnuto dugme, zameni vrednosti
//		Serial.println(digitalRead(button));
		int tmp = interval1;               // intervala blinkanja
		interval1 = interval2;
		interval2 = tmp;
	}
  
}
