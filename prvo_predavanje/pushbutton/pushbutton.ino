
int button = 2;				// Taster je povezan na pin 2
int button_state;

void setup() {
	pinMode(button, INPUT);	// Inicijalizuje pin 2 kao ulaz

	Serial.begin(9600);		// Inicijalizuje serijsku komunikaciju na brzini od 9600 bit/s
}

void loop() {
	button_state = digitalRead(button);	// Ocitava stanje tastera
	Serial.println(button_state);		// Stampa stanje tastera
	delay(1);							// Kratka pauza izmedju ocitavanja
}
